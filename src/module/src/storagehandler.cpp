#include "storagehandler.h"
#include <cstring>
#include <cstdlib>

#ifdef DEBUG
#include <iostream>
using namespace std;
#endif

#define MEMORY_SIZE 5


StorageHandler::StorageHandler(){
	this->_rwMutex = PTHREAD_MUTEX_INITIALIZER;

	//TODO: Remove simulated memory
	this->_simulatedMemory = (uint8_t *)malloc(MEMORY_SIZE);
}

StorageHandler::~StorageHandler(){
	//TODO: Remove simulated memory
	free(this->_simulatedMemory);
}

bool StorageHandler::write(uint32_t addr, void *buff, size_t size){
	if(!this->isInRange(addr)){
		return false;
	}
	
	//Write to memory
	pthread_mutex_lock(&this->_rwMutex);
	memcpy((this->_simulatedMemory + addr), buff, size);
	pthread_mutex_unlock(&this->_rwMutex);

	return true;
}

bool StorageHandler::read(uint32_t addr, void *buff, size_t size){
	if(!this->isInRange(addr)){
		return false;
	}

	//Read from memory
	pthread_mutex_lock(&this->_rwMutex);
	memcpy(buff, (this->_simulatedMemory + addr), size);
	pthread_mutex_unlock(&this->_rwMutex);

	return true;
}

#ifdef DEBUG
void StorageHandler::printMem(){
	for(int i = 0; i < MEMORY_SIZE; i++){
		if(i == ADDR_EVENTLOG_BASE){
			cout << "\nEVENTLOG: \n";
		}else if(i == ADDR_MODULES_BASE){
			cout << "\nMODULES: \n";
		}else if(i == ADDR_CONFIG_BASE){
			cout << "\nCONFIG: \n";
		}

		if(i % 128 == 0){
			cout << "\n" << hex << i << dec << ":\t";
		}
		cout << hex << int((uint8_t)this->_simulatedMemory[i]) << dec;
	}
}
#endif

bool StorageHandler::isInRange(uint32_t addr){
	return (addr <= MEMORY_SIZE-1);
}
