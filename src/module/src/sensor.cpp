#include "sensor.h"

// Macro to simulate reading ditigal values
#define digitalRead(x) 1

#include <iostream>
using namespace std;

Sensor::Sensor(Config *conf, int jumperPin0, int jumperPin1, int jumperPin2, int jumperPin3, int dataPin):
	_jumperPin0(jumperPin0),
	_jumperPin1(jumperPin1),
	_jumperPin2(jumperPin2),
	_jumperPin3(jumperPin3),
	_dataPin(dataPin),
	_conf(conf)
{}

SensorType Sensor::getSensorType(){
	// return 	(SensorType)
	// 	(digitalRead(this->_jumperPin0) |
	// 	digitalRead(this->_jumperPin1) << 1 |
	// 	digitalRead(this->_jumperPin2) << 2 |
	// 	digitalRead(this->_jumperPin3) << 3);
	return SensorType::MQ135;
}

int Sensor::_calcualteMQValue(int resistance, int dataPin){

	int adc = digitalRead(dataPin);
	return 5 - (adc * 5 / (2^(16)) ) / adc * 5 / (2^(16)) ;
}

uint32_t Sensor::getSensorValue(){

	uint32_t value = 0;

	switch(this->getSensorType()){
		case MQ2:
			value = _calcualteMQValue(9, _dataPin);
			break;
		case MQ3:
			value = _calcualteMQValue(9, _dataPin);
			break;
		case MQ4:
			value = _calcualteMQValue(9, _dataPin);
			break;
		case MQ5:
			value = _calcualteMQValue(25, _dataPin);
			break;
		case MQ6:
			value = _calcualteMQValue(25, _dataPin);
			break;
		case MQ7:
			value = _calcualteMQValue(9, _dataPin);
			break;
		case MQ8:
			value = _calcualteMQValue(25, _dataPin);
			break;
		case MQ9:
			value = _calcualteMQValue(25, _dataPin);
			break;
		case MQ135:
			value = _calcualteMQValue(85, _dataPin);
			break;
		default:
			value = -1;
			break;
	}
	return value;
}

bool Sensor::hasExceededThresshold(){
	uint32_t threshold = this->_conf->getThreshold();
	uint32_t sensorValue = getSensorValue();

	if(sensorValue >= threshold)
		return true;
	return false;
}
