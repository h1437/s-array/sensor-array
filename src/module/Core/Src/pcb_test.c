#include "pcb_test.h"

void pcbTestInit(){
	HAL_GPIO_WritePin(UART_RTS_GPIO_Port, UART_RTS_Pin, GPIO_PIN_SET);
}

void testPCB(UART_HandleTypeDef *huart) {
	testLeds();
	HAL_Delay(PAUSE_BETWEEN_TESTS);

	testBuzzer();
	HAL_Delay(PAUSE_BETWEEN_TESTS);

	testUART(huart);
	HAL_Delay(PAUSE_BETWEEN_TESTS);

	//testADC();
	//HAL_Delay(PAUSE_BETWEEN_TESTS);
}

void testLeds() {
	HAL_GPIO_TogglePin(LED_R_GPIO_Port, LED_R_Pin);
	HAL_Delay(LED_PULSE_TIME);
	HAL_GPIO_TogglePin(LED_R_GPIO_Port, LED_R_Pin);

	HAL_GPIO_TogglePin(LED_G_GPIO_Port, LED_G_Pin);
	HAL_Delay(LED_PULSE_TIME);
	HAL_GPIO_TogglePin(LED_G_GPIO_Port, LED_G_Pin);

	HAL_GPIO_TogglePin(LED_B_GPIO_Port, LED_B_Pin);
	HAL_Delay(LED_PULSE_TIME);
	HAL_GPIO_TogglePin(LED_B_GPIO_Port, LED_B_Pin);
}

void testBuzzer() {
	HAL_GPIO_TogglePin(BUZZER_GPIO_Port, BUZZER_Pin);
	HAL_Delay(BUZZER_PULSE_TIME);
	HAL_GPIO_TogglePin(BUZZER_GPIO_Port, BUZZER_Pin);
}

void testUART(UART_HandleTypeDef *huart) {
	uint8_t buff[] = "Hello World!\n";


	GPIO_PinState state = HAL_GPIO_ReadPin(UART_CTS_GPIO_Port, UART_CTS_Pin);
	if(state == GPIO_PIN_SET){
		return;
	}

	HAL_GPIO_WritePin(UART_RTS_GPIO_Port, UART_RTS_Pin, GPIO_PIN_RESET);
	HAL_StatusTypeDef status = HAL_UART_Transmit(huart, buff, sizeof(buff), 2000);
	HAL_GPIO_WritePin(UART_RTS_GPIO_Port, UART_RTS_Pin, GPIO_PIN_SET);


	//status = HAL_UART_Transmit_IT(huart, buff, sizeof(buff));

}

void testADC() {
	HAL_StatusTypeDef ret;
	double ADC_Value = 0.0;
	uint8_t buf[16];
	int16_t val;

	//Create I2C hal object
	I2C_HandleTypeDef hi2c1;

	//Setup ADC
	buf[0] = ADC_config;
	buf[1] = ADC_config_setting1;
	buf[2] = ADC_config_setting2;

	ret = HAL_I2C_Master_Transmit(&hi2c1, ADC_ADDR_SDA, buf, 6, HAL_MAX_DELAY);
	if ( ret != HAL_OK ) {
		return;
	}

	
	//Communicate with the ADC
	buf[0] = ADC_conversion;

	ret = HAL_I2C_Master_Transmit(&hi2c1, ADC_ADDR_SDA, buf, 2, HAL_MAX_DELAY);
	if (ret != HAL_OK) {
		return;
	}

	ret = HAL_I2C_Master_Receive(&hi2c1, ADC_ADDR_SDA, buf, 4, HAL_MAX_DELAY);
	if (ret != HAL_OK) {
		return;
	}

	//Calculate ADC value
	val = ((int16_t) buf[0]);
	val = ((int16_t) buf[0] << 8 | (buf[1] >> 8));
	ADC_Value = val * (2.048 / 32767.0);
}

void testSensorType() {

}
