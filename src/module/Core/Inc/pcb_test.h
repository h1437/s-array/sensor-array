/*
 * pcb_test.h
 *
 *  Created on: Jan 14, 2021
 *      Author: wouter
 */

#ifndef INC_PCB_TEST_H
#define INC_PCB_TEST_H

#include "stm32wbxx_hal.h"
#include "main.h"

#define PAUSE_BETWEEN_TESTS 500
#define LED_PULSE_TIME 500
#define BUZZER_PULSE_TIME 100

static const uint16_t ADC_ADDR_SDA = 0x48<<1;
static const uint8_t ADC_conversion = 0x00; // Use 8-bit address
static const uint8_t ADC_config = 0x01;
static const uint16_t ADC_config_setting1 = 0x00;
static const uint16_t ADC_config_setting2 = 0x80;

void pcbTestInit();
void testPCB(UART_HandleTypeDef *huart);
void testLeds();
void testBuzzer();
void testUART(UART_HandleTypeDef *huart);
void testADC();
void testSensorType();




#endif
