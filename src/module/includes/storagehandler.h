#ifndef STORAGEHANDLER_H 
#define STORAGEHANDLER_H 

#include <cstdint>
#include <stddef.h>
#include <pthread.h>

#define ADDR_EVENTLOG_BASE	0x00000
#define ADDR_MODULES_BASE	0xC0001
#define ADDR_CONFIG_BASE	0xD0000

class StorageHandler{
	public:
		StorageHandler();
		~StorageHandler();

		bool write(uint32_t addr, void *buff, size_t size);
		bool read(uint32_t addr, void *buff, size_t size);
	
#ifdef DEBUG
		void printMem();
#endif



	private:
		bool isInRange(uint32_t addr);
		uint8_t *_simulatedMemory;

		//Variables
		pthread_mutex_t _rwMutex;

};

#endif
