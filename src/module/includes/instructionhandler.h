#ifndef INSTRUCTION_HANDLER_H
#define INSTRUCTION_HANDLER_H

#include <cstdint>
#include <cstring>

#include "config.h"
#include "sensor.h"

class InstructionHander{
	public:
		InstructionHander(Config *config, Sensor *sensor);

		void sendStreamData();
		void readData();
		void executeInstruction();

	private:
};
#endif
