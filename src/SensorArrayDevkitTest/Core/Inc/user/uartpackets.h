#ifndef UART_PACKETS_H 
#define UART_PACKETS_H 

#include <cstdint>
#include <cstring>

#define UART_PACKETID_STOP_STREAM 0x21
#define SOFTWARE_VERSION "1.0"
#define DEVICE_TYPE 0
#define MODULE_ID 1


//Pack all structs
#pragma pack(push, 1)


/**
 * \brief The UARTPacket base struct
 */
struct UARTPacket{};

/**
 * \brief UART Packet with id
 *
 * Can also be used as ACK packet
 */
struct UARTInstructionPacket : UARTPacket{
	uint8_t instruction_id;
	UARTInstructionPacket(uint8_t id) : instruction_id(id){};
};




/******************** Instruction 0x01 ********************/
/**
 * \brief TX Packet with the amount of events to be read. Will be send when instructed to send entries from the eventlog
 */
struct EventRead_TX : UARTInstructionPacket{
	EventRead_TX() : UARTInstructionPacket(0x01) {};
	uint16_t eventCount;
};

/**
 * \brief TX Packet with a single entry from the eventlog. More than one can be send after EventRead_TX has been send
 */
struct EventReadData_TX : UARTPacket{
	uint16_t id;
	uint16_t sensorModuleId;
	uint32_t timestamp;
	uint32_t sensorValue;
};




/******************** Instruction 0x02 ********************/
/**
 * \brief RX Packet. Instructs the device to delete the specified event
 */
struct EventDelete_RX : UARTInstructionPacket{
	EventDelete_RX() : UARTInstructionPacket(0x02) {};
	uint16_t id;
};

/**
 * \brief TX Packet. Will be send after an event has been deleted 
 */
struct EventDelete_TX : UARTInstructionPacket{
	EventDelete_TX() : UARTInstructionPacket(0x02) {};
	uint16_t id;
};




/******************** Instruction 0x03 ********************/
/**
 * \brief TX Packet with the amount of known modules
 */
struct GetModulesList_TX : UARTInstructionPacket{
	GetModulesList_TX() : UARTInstructionPacket(0x03) {};
	uint16_t moduleCount;
};

/**
 * \brief TX Packet with a single module id entry
 */
struct GetModulesListData_TX : UARTPacket{
	uint16_t sensorModuleId;
};




/******************** Instruction 0x10 ********************/
/** 
 * \brief TX Packet with the current timestamp on this device
 */
struct GetConfigTime_TX : UARTInstructionPacket{
	GetConfigTime_TX() : UARTInstructionPacket(0x10) {};
	uint32_t timestamp;
};




/******************** Instruction 0x11 ********************/
/**
 * \brief RX Packet with a new timestamp to update the RTC on the device
 */
struct SetConfigTime_RX : UARTInstructionPacket{
	SetConfigTime_RX() : UARTInstructionPacket(0x11) {};
	uint32_t timestamp;
};




/******************** Instruction 0x12 ********************/
/**
 * \brief TX Packet with the alarm config of this device
 */
struct GetAlarmConfig_TX : UARTInstructionPacket{
	GetAlarmConfig_TX() : UARTInstructionPacket(0x12) {};
	uint8_t alarmConfig;
};




/******************** Instruction 0x13 ********************/
/**
 * \brief RX Packet with the new alarm config for this device
 */
struct SetAlarmConfig_RX : UARTInstructionPacket{
	SetAlarmConfig_RX() : UARTInstructionPacket(0x13) {};
	uint8_t alarmConfig;
};




/******************** Instruction 0x14 ********************/
/**
 * \brief RX Packet with the sensortype to receive the configured threshold value from
 */
struct GetSensorThreshold_RX : UARTInstructionPacket{
	GetSensorThreshold_RX() : UARTInstructionPacket(0x14) {};
	uint8_t sensorType;
};

/**
 * \brief TX Packet with the threshold value of the specified sensortype in GetSensorThreshold_RX
 */
struct GetSensorThreshold_TX : UARTInstructionPacket{
	GetSensorThreshold_TX() : UARTInstructionPacket(0x14) {};
	uint32_t value;
};




/******************** Instruction 0x15 ********************/
/**
 * \brief RX Packet with the new thresholdvalue to store in the config for the specified sensortype
 */
struct SetSensorThreshold_RX : UARTInstructionPacket{
	SetSensorThreshold_RX() : UARTInstructionPacket(0x15) {};
	uint8_t sensorType;
	uint32_t thresholdValue;
};




/******************** Instruction 0x16 ********************/
/**
 * \brief RX Packet with the sensor module id to receive the module config from
 */
struct GetModuleConfig_RX : UARTInstructionPacket{
	GetModuleConfig_RX() : UARTInstructionPacket(0x16) {};
	uint16_t sensorModuleId;
};

/**
 * \brief TX Packet with the configuration from the specified module id in GetModuleConfig_RX
 */
struct GetModuleConfig_TX : UARTInstructionPacket{
	GetModuleConfig_TX() : UARTInstructionPacket(0x16) {};
	uint16_t sensorModuleId;
	uint8_t alarmConfig;
	uint8_t sensorType;
};




/******************** Instruction 0x17 ********************/
/**
 * \brief RX Packet with a new module config
 */
struct SetModuleConfig_RX : UARTInstructionPacket{
	SetModuleConfig_RX() : UARTInstructionPacket(0x17) {};
	uint16_t sensorModuleId;
	uint8_t alarmConfig;
};

/**
 * \brief TX Packet send as ACK when SetModuleConfig_RX has been received
 */
struct SetModuleConfig_TX : UARTInstructionPacket{
	SetModuleConfig_TX() : UARTInstructionPacket(0x17) {};
	uint16_t sensorModuleId;
	uint8_t status;
};




/******************** Instruction 0x20 ********************/
/**
 * \brief RX Packet with sensor module id to start a datastream on
 */
struct StartStream_RX : UARTInstructionPacket{
	StartStream_RX() : UARTInstructionPacket(0x20) {};
	uint16_t sensorModuleId;
};

/**
 * \brief TX Packet send when one or more modules is streaming data
 */
struct StreamData_TX : UARTPacket{
	uint32_t timestamp;
	uint16_t sensorModuleId;
	uint8_t sensorType;
	uint32_t sensorValue;
};




/******************** Instruction 0xFE ********************/
/**
 * \brief RX Packet. Instructs the device to perform a factory reset
 */
struct FactoryReset : UARTInstructionPacket{
	FactoryReset() : UARTInstructionPacket(0xFE) {};
};




/******************** Instruction 0xFF ********************/
/**
 * \brief TX Packet with device information
 */
struct GetDeviceInformation_TX : UARTInstructionPacket{
	GetDeviceInformation_TX() : UARTInstructionPacket(0xFF){
		memset(softwareVersion, 0, 11);
		strcpy(softwareVersion, SOFTWARE_VERSION);
		strcpy(lastUpdateDate, __DATE__);
		strcpy(lastUpdateTime, __TIME__);
		deviceType = DEVICE_TYPE;
		sensorModuleId = MODULE_ID;

	};
	char softwareVersion[11];//= SOFTWARE_VERSION;
	char lastUpdateDate[11];// = __DATE__;
	char lastUpdateTime[8];// = __TIME__;
	uint8_t deviceType; 
	uint16_t sensorModuleId;
};


//Disable struct packing
#pragma pack(pop)
#endif
