#!/usr/bin/env python3

import csv
import time
import getopt
import sys
sys.path.insert(0, "../")
from salib import *

def usage():
    usage_page = open('usage_page.txt', 'r')
    print(usage_page.read())

def check_parameters_empty():
    if len(sys.argv) == 1:
        usage()
        sys.exit(2)

def check_port_param_used(options):

    help_par_used = False
    port_par_used = False

    for par in options:
        if '-p' in par or '--port' in par:
            port_par_used = True
        if '-h' in par or '--help' in par:
            help_par_used = True

    if not port_par_used and not help_par_used:
        raise Exception('Port not specified')
        sys.exit(2)

#def combine_double_parameters():
#    args = sys.argv[1:]
#
#    for i in sys.argv:
#        if '-' not in i:
#            print(i)
#
#    return args

def get_options():

    try:
        options, remainder = getopt.gnu_getopt(
            sys.argv[1:],
            'p:ys:g:d:m:h',
            ['port=',
             'synctime',
             'set=',
             'get=',
             'delete=',
             'measure=',
             'help',
            ]
                                            )
    except getopt.GetoptError as err:
        print(str(err))
        sys.exit(2)

    return options, remainder

def get_params(options):

    params = {
        "port": None,
        "sync_time": False,
        "set": None,
        "get": None,
        "delete_event": None,
        "measure": None
    }

    for opt, arg in options:
        if opt in ('-p', '--port'):
            params['port'] = arg
        elif opt in ('-y', '--synctime'):
            params['sync_time'] = True
        elif opt in ('-s', '--set'):
            params['set'] = arg.split(':')
        elif opt in ('-g', '--get'):
            params['get'] = arg.split(':')
        elif opt in ('-d', '--delete'):
            params['delete_event'] = arg
        elif opt in ('-m', '--measure'):
            params['measure'] = arg.split(':')
        elif opt in ('-h', '--help'):
            usage()

    return params

def setup_connection(salib, params):

    try:
        is_connected = salib.openSerial(params['port'])
    except:
        print("Serial error")

    if not is_connected:
        print("Serial can not connect")
        sys.exit(2)
    else:
        #print("Serial connected")
        pass

def set_config(salib, args):

    succes = None

    # Handle set function
    if args != None:
        if args[0] == "alarm-config":
            succes = salib.setAlarmConfig(1 if bool(args[1]) else 0,
                                 1 if bool(args[2]) else 0,)
        elif args[0] == "module-config":
            succes = salib.setModuleConfig(int(args[1]),
                                  1 if bool(args[2]) else 0,
                                  1 if bool(args[3]) else 0,)
        elif args[0] == "threshold":
            succes = salib.setSensorThreshold(
                                int(args[1][2:]), # Remove mq, only send number
                                int(args[2]),)
        else:
            print("Set option not found")
            sys.exit(2)

    if succes:
        print('Config set')
    else:
        print('Config error from server')

def get_config(salib, args):

    if args != None:
        if args[0] == "events":
            r = salib.getEvents()
            for i in r:
                print(i)
        elif args[0] == "modules":
            print(str(salib.getModulesList()))
        elif args[0] == "thresholds":
            print(salib.getSensorThreshold(
                int(args[1][2:]))
            )
        elif args[0] == "module-config":
            r = salib.getModuleConfig(int(args[1]))
            for key, value in r.items():
                print(key, value)

        elif args[0] == "alarm-config":
            r = salib.getModuleConfig(int(args[1]))
            for key, value in r.items():
                print(key, value)

        elif args[0] == "device-info":
            r = salib.getDeviceInformation()
            for key, value in r.items():
                print(key, value)
        else:
            print(f"Get parameter does not exist: '{args[0]}'")
            sys.exit(2)

def measure(salib, args):

    data_arr = list()

    init_time = time.perf_counter()

    salib.startDataStream();

    # wait for counter to run out or run forever if parameter is < 0
    #while(time.perf_counter() - init_time < int(args[2]) or int(args[2] <= -0):
    # TODO, or functio not working?
    while(time.perf_counter() - init_time < int(args[2])):
        data = salib.readDataStream()

        if data:
            print(data)
            data_arr.append(data)
        # else:
        #     data_arr.append('testdata, 32')
    
    salib.stopDataStream();

    with open(args[0], 'w') as csv_file:
        writer = csv.writer(csv_file)
        for item in data_arr:
           writer.writerow(item.split(","))
           print(item)

def run_params(params):

    salib = SALib()
    setup_connection(salib, params)

    if params['sync_time']: 
        salib.syncTime()

    if params['set'] is not None:
        set_config(salib, params['set'])

    if params['get'] is not None:
        get_config(salib, params['get'])

    if params['delete_event'] is not None:
        salib.deleteEvent(params['delete_event'])

    if params['measure'] is not None:
        measure(salib, params['measure'])

def main():
    check_parameters_empty()
    options, remainder = get_options()
    check_port_param_used(options)
    params = get_params(options)
    run_params(params)

if __name__ == '__main__':
    main()
