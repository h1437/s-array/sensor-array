PacketTypes = {
    "READ_EVENT": {
        "id": 0x01,
        "uplink": {
            "size": 1,
            "format": "B"
        },
        "downlink": {
            "size": 3,
            "format": "BH"
        }
    },
    "READ_EVENT_DATA":{
        "id": None,
        "uplink": None,
        "downlink": {
            "size": 12,
            "format": "HHII"
        }
    },
    "DELETE_EVENT":{
        "id": 0x02,
        "uplink": {
            "size": 3,
            "format": "BH"
        },
        "downlink":{
            "size": 3,
            "format": "BH"
        }
    },
    "GET_MODULES_LIST":{
        "id": 0x03,
        "uplink": {
            "size": 1,
            "format": "B"
        },
        "downlink": {
            "size": 3,
            "format": "BH"
        }
    },
    "GET_MODULES_LIST_DATA":{
        "id": None,
        "uplink": None,
        "downlink": {
            "size": 2,
            "format": "H"
        }
    },
    "GET_TIME":{
        "id": 0x10,
        "uplink": {
            "size": 1,
            "format": "B"
        },
        "downlink": {
            "size": 5,
            "format": "BI"
        }
    },
    "SET_TIME":{
        "id": 0x11,
        "uplink": {
            "size": 5,
            "format": "BI"
        },
        "downlink": {
            "size": 1,
            "format": "B"
        }
    },
    "GET_ALARM_CONFIG":{
        "id": 0x12,
        "uplink": {
            "size": 1,
            "format": "B"
        },
        "downlink": {
            "size": 2,
            "format": "BB"
        }
    },
    "SET_ALARM_CONFIG":{
        "id": 0x13,
        "uplink": {
            "size": 2,
            "format": "BB"
        },
        "downlink": {
            "size": 1,
            "format": "B"
        }
    },
    "GET_SENSOR_THRESHOLD":{
        "id": 0x14,
        "uplink": {
            "size": 2,
            "format": "BB"
        },
        "downlink": {
            "size": 5,
            "format": "BI"
        }
    },
    "SET_SENSOR_THRESHOLD":{
        "id": 0x15,
        "uplink": {
            "size": 6,
            "format": "BBI"
        },
        "downlink": {
            "size": 1,
            "format": "B"
        }
    },
    "GET_MODULE_CONFIG":{
        "id": 0x16,
        "uplink": {
            "size": 3,
            "format": "BH"
        },
        "downlink": {
            "size": 5,
            "format": "BHBB"
        }
    },
    "SET_MODULE_CONFIG":{
        "id": 0x17,
        "uplink": {
            "size": 4,
            "format": "BHB"
        },
        "downlink": {
            "size": 4,
            "format": "BHB"
        }
    },
    "START_READING_DATA":{
        "id": 0x20,
        "uplink": {
            "size": 3,
            "format": "BH"
        },
        "downlink": {
            "size": 1,
            "format": "B"
        }
    },
    "READING_DATA_STREAM":{
        "id": None,
        "uplink": None,
        "downlink": {
            "size": 11,
            "format": "IHBI"
        }
    },
    "STOP_READING_DATA":{
        "id": 0x21,
        "uplink": {
            "size": 1,
            "format": "B"
        },
        "downlink": {
            "size": 1,
            "format": "B"
        }
    },
    "FACTORY_RESET":{
        "id": 0xFE,
        "uplink": {
            "size": 1,
            "format": "B"
        },
        "downlink": {
            "size": 1,
            "format": "B"
        }
    },
    "GET_DEVICE_INFO":{
        "id": 0xFF,
        "uplink": {
            "size": 1,
            "format": "B"
        },
        "downlink": {
            "size": 34,
            "format": "B11s11s8sBH"
        }
    }
}

def getPacketType(name):
    return PacketTypes[name]
