#ifndef MODULE_H 
#define MODULE_H 

#include <cstdint>
#include "sensortype.h"
#include "alarm.h"


/**
 * \brief Struct for a module 
 */
struct Module{
	uint16_t id;
	SensorType sensorType;
	AlarmConfig alarmConfig;
};

#endif
