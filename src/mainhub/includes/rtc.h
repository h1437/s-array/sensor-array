#ifndef RTC_H 
#define RTC_H 

#include <cstdint>

/**
 * \brief Realtime clock HAL class
 */
class RTC{
	public:
		/**
		 * \brief Constructor
		 */
		RTC();

		/**
		 * \brief Deconstructor 
		 */
		~RTC();
		
		/**
		 * \brief Get the unix epoch time in seconds
		 *
		 * @return Time in seconds
		 */
		uint32_t getTimestamp();

		/**
		 * \brief Update the epoch time on the RTC
		 *
		 * @return Update was successful
		 */
		bool setTimestamp(uint32_t timestamp);

	private:
#ifdef UNITTEST
		uint32_t _timestamp;
#endif
};

#endif
