#ifndef INSTRUCTION_HANDLER_H
#define INSTRUCTION_HANDLER_H

#include <cstdint>
#include <cstring>

#include "uartpackets.h"
#include "config.h"
#include "eventlog.h"
#include "modulehandler.h"

#define UART_RX_BUFFER_SIZE sizeof(GetDeviceInformation_TX)


/**
 * \brief Handler for UART instructions from the SALib
 */
class InstructionHandler{
	public:
		//TODO: Add uart obj
		/**
		 * \brief Constructor
		 *
		 * @param config[in] Pointer to a Config object
		 * @param eventLog[in] Pointer to a EventLog object
		 * @param moduleHandler[in] Pointer to a ModuleHandler object
		 * @param rtc[in] Pointer to a RTC object
		 */
		InstructionHandler(Config *config, EventLog *eventLog, ModuleHandler *moduleHandler, RTC *rtc);

		/**
		 * \brief Deconstructor
		 */
		~InstructionHandler();
		
		/**
		 * \brief Write stream data to UART if available
		 *
		 * Blocking function. Should be executed on a thread.
		 */
		void sendStreamData();

		/**
		 * \brief Read input data from UART 
		 *
		 * Blocking function. Should be executed on a thread.
		 */
		void readData();

		/**
		 * \brief Execute a received UART instruction
		 *
		 * Should be executed right after readData()
		 */
		void executeInstruction();
		
		//TODO: set these functions to be private
		bool _canExecuteInstruction(uint8_t instructionId);
		uint8_t _getInstructionId(uint8_t *buff);
		bool _sendPacket(UARTPacket *p, size_t size);
		bool _sendACK();
	

		//Testing variables
		uint8_t _txBuffer[50];
		int _txBufferSize;
		uint8_t _lastInstructionId;

		uint8_t _rxBuffer[UART_RX_BUFFER_SIZE];
		uint8_t _rxByteCount;
	private:
		void _handleEventRead();
		void _handleEventDelete(EventDelete_RX *rx);
		void _handleGetModulesList();
		void _handleGetConfigTime();
		void _handleSetConfigTime(SetConfigTime_RX *rx);
		void _handleGetAlarmConfig();
		void _handleSetAlarmConfig(SetAlarmConfig_RX *rx);
		void _handleGetSensorThreshold(GetSensorThreshold_RX *rx);
		void _handleSetSensorThreshold(SetSensorThreshold_RX *rx);
		void _handleGetModuleConfig(GetModuleConfig_RX *rx);
		void _handleSetModuleConfig(SetModuleConfig_RX *rx);
		void _handleStartStream(StartStream_RX *rx);
		void _handleStopStream();
		void _handleFactoryReset();
		void _handleGetDeviceInformation();

		//Variables
		//uint8_t _lastInstructionId;

		//Objects
		Config *_config;
		EventLog *_eventLog;
		ModuleHandler *_moduleHandler;
		RTC *_rtc;
	

		

		
};

#endif
