#ifndef CONFIG_H
#define CONFIG_H 

#include <cstdint>
#include "sensortype.h"
#include "storagehandler.h"
#include "alarmconfig.h"


/**
 * \brief Read and write config entries
 */
class Config{
	public:
		/**
		 * \brief Constructor
		 *
		 * @param storage[in] Pointer to a StorageHandler object
		 */
		Config(StorageHandler *storage);

		/**
		 * \brief Deconstructor
		 */
		~Config();
	
		/**
		 * \brief Set the sensor threshold
		 *
		 * @param type[in] SensorType
		 * @param value[in] The new sensor threshold
		 * @return Storing the new value was successful
		 */
		bool setThreshold(SensorType type, uint32_t value);

		/**
		 * \brief Get the sensor threshold
		 *
		 * @param type[in] The SensorType to get the threshold value from
		 * @return The threshold value
		 */
		uint32_t getThreshold(SensorType type);
		
		/**
		 * \brief Set the AlarmConfig
		 * 
		 * @param config[in] The new Alarm config
		 * @return Storing the new value was successful
		 */
		bool setAlarmConfig(AlarmConfig config);

		/**
		 * \brief Get the AlarmConfig
		 *
		 * @return The AlarmConfig
		 */
		AlarmConfig getAlarmConfig();

		 /**
		 * \brief Restore the factory defaults
		 *
		 * Should also be executed before the first run of the application to initialize the memory layout
		 * 
		 * @return Restoring defaults was successful
		 */
		bool restoreFactoryDefaults();

	private:
		StorageHandler *_storage;
};

#endif
