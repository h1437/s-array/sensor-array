#include "instructionhandler.h"

#include <iostream>
using namespace std;

InstructionHandler::InstructionHandler(Config *config, EventLog *eventLog, ModuleHandler *moduleHandler, RTC *rtc):
	_config(config),
	_eventLog(eventLog),
	_moduleHandler(moduleHandler),
	_rtc(rtc)
{
	//TODO:
}

InstructionHandler::~InstructionHandler(){
	//TODO
}

void InstructionHandler::sendStreamData(){
	StreamData_TX sd;
	sd.timestamp = this->_rtc->getTimestamp();
	
	if(this->_moduleHandler->getStreamData(&sd.sensorModuleId, &sd.sensorValue, &sd.sensorType)){
		//If stream data is received, check if stream is still running
		if(this->_moduleHandler->getStreamState() == StreamState::RUNNING){
			this->_sendPacket(&sd, sizeof(sd));
		}
	}
}


//Check if an instruction is available, blocking function
void InstructionHandler::readData(){
	//TODO: Change code for UART HAL
	
	char c;
	cout << "Press a key and enter to test the instructionhandler\n";
	cin >> c;

	if(c == '0'){
		uint8_t p[1] = {0x21};
		memcpy(this->_rxBuffer, &p, sizeof(p));
	}else if(c == '1'){
		StartStream_RX p;
		p.sensorModuleId = 1;
		memcpy(this->_rxBuffer, &p, sizeof(p));
	}else if(c == '2'){
		uint8_t buff[] = {0x01};
		memcpy(this->_rxBuffer, &buff, sizeof(buff));
	}else if(c == '3'){
		EventDelete_RX p;
		p.id = 5;
		memcpy(this->_rxBuffer, &p, sizeof(p));
	}
}

void InstructionHandler::executeInstruction(){
	this->_lastInstructionId = this->_getInstructionId(this->_rxBuffer);

	//cout << "\nInstruction ID: 0x" << hex << int(this->_lastInstructionId) << dec <<"\n";
	
	//Check if instructions can be executed
	if(!this->_canExecuteInstruction(this->_lastInstructionId)){
		return;
	}
	
	//Execute the instruction
	switch(this->_lastInstructionId){
		case 0x01:
			this->_handleEventRead();
			break;
		case 0x02:
			this->_handleEventDelete((EventDelete_RX *)this->_rxBuffer);
			break;
		case 0x03:
			this->_handleGetModulesList();
			break;
		case 0x10:
			this->_handleGetConfigTime();
			break;
		case 0x11:
			this->_handleSetConfigTime((SetConfigTime_RX *)this->_rxBuffer);
			break;
		case 0x12:
			this->_handleGetAlarmConfig();
			break;
		case 0x13:
			this->_handleSetAlarmConfig((SetAlarmConfig_RX *)this->_rxBuffer);
			break;
		case 0x14:
			this->_handleGetSensorThreshold((GetSensorThreshold_RX *)this->_rxBuffer);
			break;
		case 0x15:
			this->_handleSetSensorThreshold((SetSensorThreshold_RX *)this->_rxBuffer);
			break;
		case 0x16:
			this->_handleGetModuleConfig((GetModuleConfig_RX *)this->_rxBuffer);
			break;
		case 0x17:
			this->_handleSetModuleConfig((SetModuleConfig_RX *)this->_rxBuffer);
			break;
		case 0x20:
			this->_handleStartStream((StartStream_RX *)this->_rxBuffer);
			break;
		case UART_PACKETID_STOP_STREAM:
			this->_handleStopStream();
			break;
		case 0xFE:
			this->_handleFactoryReset();
			break;
		case 0xFF:
			this->_handleGetDeviceInformation();
			break;
		default:
			break;
	}
}

bool InstructionHandler::_canExecuteInstruction(uint8_t instructionId){
	if(instructionId == UART_PACKETID_STOP_STREAM){
		//Instruction can always be executed when the stream should be stopped
		return true;
	}

	switch(this->_moduleHandler->getStreamState()){
		case StreamState::RUNNING:
			return false;
		case StreamState::NOT_RUNNING:
			//No stream is running or queued. Instruction can be executed
			break;
		case StreamState::START_QUEUED:
			return false;
		case StreamState::STOP_QUEUED:
			return false;
		case StreamState::UNKNOWN:
			//Stream could be running. Instruction can be executed.
			break;
	}
	
	return true;
}

uint8_t InstructionHandler::_getInstructionId(uint8_t *buff){
	return (uint8_t)buff[0];
}


bool InstructionHandler::_sendPacket(UARTPacket *p, size_t size){
	uint8_t buff[size];
	memcpy(&buff, (uint8_t *)p, size);

	//Setup for unittest
	memcpy(this->_txBuffer, (uint8_t *)p, size);
	this->_txBufferSize = size;
	
	cout << "UART TX Buff[" << int(size) << "] content: {";
	for(int i = 0; i < (int)size; i++){
		cout << "0x" << hex << int((uint8_t)buff[i]) << dec << ", ";
	}
	cout << "}\n";
	return true;
}

bool InstructionHandler::_sendACK(){
	UARTInstructionPacket p(this->_lastInstructionId);
	return this->_sendPacket(&p, sizeof(p));
}

void InstructionHandler::_handleEventRead(){
	EventRead_TX p;
	EventReadData_TX data;
	Event e;

	//Send response
	p.eventCount = this->_eventLog->getEventCount();
	this->_sendPacket(&p, sizeof(p));
	
	//Loop through all events
	this->_eventLog->resetIndex();
	for(int i = 0; i < p.eventCount; i++){
		e = this->_eventLog->getEvent();
		cout << "Event: " << e.moduleId << ", " << e.timestamp << ", " << e.value << "\n";

		//Construct the packet	
		data.id = this->_eventLog->getEventNumber();
		data.sensorModuleId = e.moduleId;
		data.timestamp = e.timestamp;
		data.sensorValue = e.value;

		//Send the data
		this->_sendPacket(&data, sizeof(data));

		//Goto the next event
		this->_eventLog->nextEvent();
	}
}

void InstructionHandler::_handleEventDelete(EventDelete_RX *rx){
	EventDelete_TX p;
	p.id = rx->id;
	
	//Only send ACK if the item was deleted
	if(this->_eventLog->deleteEvent(rx->id)){
		this->_sendPacket(&p, sizeof(p));
	}
}

void InstructionHandler::_handleGetModulesList(){
	GetModulesList_TX p;
	GetModulesListData_TX data;
	Module m;
	
	//Get the amount of modules
	p.moduleCount = this->_moduleHandler->getModuleCount();

	//Send response
	this->_sendPacket(&p, sizeof(p));

	for(int i = 0; i < MODULE_ID_MAX; i++){
		if(this->_moduleHandler->moduleIsKnown(i)){

			//Module is known, send the packet
			data.sensorModuleId = i;
			this->_sendPacket(&data, sizeof(data));
		}
	}
}

void InstructionHandler::_handleGetConfigTime(){
	GetConfigTime_TX p;

	p.timestamp = this->_rtc->getTimestamp();

	//Send response
	this->_sendPacket(&p, sizeof(p));
}

void InstructionHandler::_handleSetConfigTime(SetConfigTime_RX *rx){
	if(this->_rtc->setTimestamp(rx->timestamp)){
		this->_sendACK();
	}
}

void InstructionHandler::_handleGetAlarmConfig(){
	GetAlarmConfig_TX p;
	
	//Get the alarm config struct
	AlarmConfig alarmConf = this->_config->getAlarmConfig();
	
	//Convert the struct to uint8 and send the packet
	p.alarmConfig = alarmConf.to_uint8();
	this->_sendPacket(&p, sizeof(p));
}

void InstructionHandler::_handleSetAlarmConfig(SetAlarmConfig_RX *rx){
	//Cast uint8_t to alarm config struct
	AlarmConfig alarmConf = AlarmConfig(rx->alarmConfig);
	
	//Update the config
	if(this->_config->setAlarmConfig(alarmConf)){
		//Send ACK if succeeded
		this->_sendACK();
	}
}

void InstructionHandler::_handleGetSensorThreshold(GetSensorThreshold_RX *rx){
	GetSensorThreshold_TX p;

	//Get sensor type from struct
	p.value = this->_config->getThreshold((SensorType)rx->sensorType);

	//Send response
	this->_sendPacket(&p, sizeof(p));
}


void InstructionHandler::_handleSetSensorThreshold(SetSensorThreshold_RX *rx){
	//Set the sensor threshold
	if(this->_config->setThreshold((SensorType)rx->sensorType, rx->thresholdValue)){

		//Sync all modules. This will update all devices even if the threshold value was not changed.
		this->_moduleHandler->syncAllModules();

		//Send ACK if succeeded
		this->_sendACK();
	}
}

void InstructionHandler::_handleGetModuleConfig(GetModuleConfig_RX *rx){
	GetModuleConfig_TX p;
	Module m;

	if(this->_moduleHandler->moduleIsKnown(rx->sensorModuleId)){
		m = this->_moduleHandler->getModule(rx->sensorModuleId);
		p.sensorModuleId = rx->sensorModuleId;
		p.alarmConfig = m.alarmConfig.to_uint8();
		p.sensorType = (uint8_t)m.sensorType;
		
		//Send response
		this->_sendPacket(&p, sizeof(p));
	}
}

void InstructionHandler::_handleSetModuleConfig(SetModuleConfig_RX *rx){
	SetModuleConfig_TX p;
	
	//Update the alarm config. The device will be synced
	p.status = this->_moduleHandler->setAlarmConfig(rx->sensorModuleId, AlarmConfig(rx->alarmConfig));
	
	//Send response
	p.sensorModuleId = rx->sensorModuleId;
	this->_sendPacket(&p, sizeof(p));
}

void InstructionHandler::_handleStartStream(StartStream_RX *rx){
	//Start the stream
	if(!this->_moduleHandler->startStream(rx->sensorModuleId)){
		return;
	}

	//Send the ACK packet
	this->_sendACK();
}

void InstructionHandler::_handleStopStream(){
	//Stop the stream
	if(!this->_moduleHandler->stopStream()){
		return;
	}

	//Send the ACK
	this->_sendACK();
}

void InstructionHandler::_handleFactoryReset(){
	//Perform factory resets
	if(!this->_eventLog->restoreFactoryDefaults()){
		return;
	}
	if(!this->_moduleHandler->restoreFactoryDefaults()){
		return;
	}
	if(!this->_config->restoreFactoryDefaults()){
		return;
	}
	
	//Send the ACK
	this->_sendACK();

	//TODO: reboot the device to clear all queues and restart all threads
}

void InstructionHandler::_handleGetDeviceInformation(){
	GetDeviceInformation_TX p;
	this->_sendPacket(&p, sizeof(p));
}
